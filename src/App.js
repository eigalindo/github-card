import React, {Component} from 'react';
import 'semantic-ui-css/semantic.min.css';
import { Card, Image} from "semantic-ui-react";


class App extends Component {
state = {
  user: {},
  isActive: false
};

  handleToggle = () => {
    if (this.state.isActive){
      this.setState({ isActive: false })
    }
  console.log("handleToggle was called")
  fetch('//api.github.com/users/EnriqueGalindo')
  .then(res=>res.json())
  .then(result=>{
    this.setState({ user: result, isActive: true});
  });
  };
  render(){
  return (
    <React.Fragment>
    <button onClick={this.handleToggle}>Toggle User</button>
    {this.state.isActive && (
        <Card>
        <Image src={this.state.user.avatar_url} wrapped ui={false} />
        <Card.Content>
          <Card.Header>{this.state.user.name}</Card.Header>
          <Card.Meta>
            <span className='date'>Joined in 2015</span>
          </Card.Meta>
          <Card.Description>
           {this.state.user.bio}
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
        </Card.Content>
      </Card>
      )}
    </React.Fragment>
  );
}
}

// <React.Fragment>
// <img src={this.state.user.avatar_url} />
// <p>{this.state.user.name}</p>
// <p>{this.state.user.bio}</p>
// <p>{this.state.user.followers}</p>
// </React.Fragment>
export default App;
